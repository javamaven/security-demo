package com.zhm.springboot.service.impl;

import com.zhm.springboot.dao.UserInfoDao;
import com.zhm.springboot.domain.UserInfo;
import com.zhm.springboot.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by haiming.zhuang on 2016/7/12.
 */
@Service("userInfoService")
public class UserInfoServiceImpl implements UserInfoService {
    @Autowired
    private UserInfoDao userInfoDao;

    @Override
    public List<UserInfo> findAll() {
        return userInfoDao.findAll();
    }

    @Override
    public UserInfo findById(Integer userid) {
        return userInfoDao.findById(userid);
    }

    @Override
    public void delById(Integer userid) {
        userInfoDao.delById(userid);
    }

    @Override
    public void saveUser(UserInfo user) {
        userInfoDao.saveUser(user);
    }

    @Override
    public void updateUser(UserInfo dbInfo) {
        userInfoDao.updateUser(dbInfo);
    }

    @Override
    public UserInfo findByUsername(String username) {
        return userInfoDao.findByUsername(username);
    }
}
